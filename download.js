const fetch = require('node-fetch')
const download = require('download');
const isbnList = require('./books.json');

async function downloadBook(isbn) {
  const bookPageHtml = await fetch(`http://link.springer.com/openurl?genre=book&isbn=${isbn}`).then(r => r.text());

  const links = bookPageHtml.match(/\/content\/pdf\/.*?\.pdf"/g);
  const url = `https://link.springer.com${links[0]}`;

  let title = bookPageHtml.match(/<h1>.*<\/h1>/g)[0];
  title = title.slice(4, -5);

  let authors = bookPageHtml.match(/<span itemprop="name" class="authors__name">.*?<\/span>/g);
  authors = authors.map(author => author.slice(44, -7));

  return download(
    url,
    'downloads',
    {
      filename: `${title} - ${authors.join(', ')} -- ${isbn}.pdf`
    },
  );
}

Promise.all(isbnList.map(downloadBook));
